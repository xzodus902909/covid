import "./App.css";
import TextField from "@material-ui/core/TextField";
import { useState, useEffect } from "react";
import axios from "axios";
import { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Swiper, SwiperSlide } from "swiper/react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "./style/mystyle.scss";
import "swiper/swiper.scss";
import logo from "./img/lg.gif"; // with import
import SwiperCore, { Autoplay } from "swiper";
SwiperCore.use([Autoplay]);
var country = [];
var tempdata = [];
var master_data = [];
var sort_D_all = [];
var sort_D_dead = [];
var sort_D_recover = [];
var filter_data;

var main_data = [];
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      keyword: "",
    };
    this.search_egine = this.search_egine.bind(this);
    this.sort_by_all = this.sort_by_all.bind(this);
  }
  async componentDidMount() {
    const result = await axios("https://api.covid19api.com/summary");
    const result_master = await axios("https://api.covid19api.com/summary");
    master_data = result_master.data.Countries;
    main_data = result.data.Countries;
    tempdata = result.data.Countries;
    this.setState({
      data: main_data,
    });
  }
  async shouldComponentUpdate() {
    return true;
  }

  search_egine(val) {
    this.setState({ keyword: val.target.value });
    filter_data = this.state.data.filter((auto) =>
      auto.Country.includes(val.target.value)
    );
    main_data = filter_data;
    console.log("test" + filter_data);
    console.log(main_data);
  }
  sort_by_all() {
    main_data.sort(function (a, b) {
      return b.TotalConfirmed - a.TotalConfirmed;
    });
    this.setState({ data: main_data });
    console.log(main_data);
  }
  sort_by_dead() {
    main_data.sort(function (a, b) {
      return b.TotalDeaths - a.TotalDeaths;
    });
    this.setState({ data: main_data });
    console.log(main_data);
  }
  sort_by_recovered() {
    main_data.sort(function (a, b) {
      return b.TotalRecovered - a.TotalRecovered;
    });
    this.setState({ data: main_data });
    console.log(main_data);
  }
  sort_by_name() {
    main_data.sort(function (a, b) {
      return a.Country.localeCompare(b.Country);
    });
    this.setState({ data: main_data });
    console.log(main_data);
  }
  render() {
    return (
      <div className="App">
        <div className="logo">
          <img src={logo} alt="Logo" />
        </div>
        <div className="white-box">
          <form noValidate autoComplete="off">
            <TextField
              id="outlined-basic"
              label="Search"
              variant="outlined"
              className="search"
              onChange={(val) => this.search_egine(val)}
            />
          </form>
        </div>

        <div className="box">
          <Swiper
            spaceBetween={500}
            slidesPerView={1}
            autoplay={{ delay: 3000 }}
            onSlideChange={() => console.log("this.state.data")}
            onSwiper={(swiper) => console.log(swiper)}
          >
            {main_data.map((row) => (
              <SwiperSlide>
                <Card className="root">
                  <CardContent>
                    <Typography
                      className="title"
                      color="textSecondary"
                      gutterBottom
                    >
                      {row.Country}
                    </Typography>
                    <Typography variant="h5" component="h2">
                      Total cases : {row.TotalConfirmed}
                    </Typography>

                    <Typography variant="body2" component="p">
                      Total death cases : {row.TotalDeaths}
                      <br />
                      Total recovered cases : {row.TotalRecovered}
                    </Typography>
                    <Typography className="pos" color="textSecondary">
                      Date : {Date(row.Date)}
                    </Typography>
                  </CardContent>
                </Card>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
        <Button variant="contained" onClick={() => this.sort_by_name()}>
          All
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={() => this.sort_by_all()}
        >
          Sort by total case
        </Button>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => this.sort_by_dead()}
        >
          Sort by dead case
        </Button>
        <Button
          variant="contained"
          color="primary"
          href="#contained-buttons"
          onClick={() => this.sort_by_recovered()}
        >
          Sort by recovered case
        </Button>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Country</TableCell>
                <TableCell align="right">Total case</TableCell>
                <TableCell align="right">Total dead case</TableCell>
                <TableCell align="right">Total recorvered case</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {main_data.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.Country}
                  </TableCell>
                  <TableCell align="right">{row.TotalConfirmed}</TableCell>
                  <TableCell align="right">{row.TotalDeaths}</TableCell>
                  <TableCell align="right">{row.TotalRecovered}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}
